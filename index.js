const express = require('express')

const bodyParser = require('body-parser')


const PORT = 8080 || process.env.PORT
const app =  express()

app.use(bodyParser.json()) // converts req bodies to json

// super car API

let cars = 
[
	{ make: 'Fiat', model: 'Uno', year: '2004', id: 1 },
  	{ make: 'Ford', model: 'Mustang', year: '1969', id: 2 },
  	{ make: 'Tesla', model: 'Model 3', year: '2021', id: 3 }
]

//Create
app.post('/car', (req, res) => {

	let car = req.body
	if(car)
	{
		car.id = (cars.length+1)
		cars.push(car)
	}

	res.status(200).send(car)

	console.log(cars)
})

//Read
app.get('/car/:id',(req, res) => {

	let car = null
	let id = req.params.id
	for (var i = cars.length - 1; i >= 0; i--) {
		if(cars[i].id == id)
		{
			car = cars[i]
		}
	}
	if(car == null)
		{
			res.status(404)
		}
	else
		{
			res.status(200)
		}
	res.send(car)
})

//Read all
app.get('/car',(req, res) => {
	res.status(200)
	res.send(cars)
})

//Update
app.put('/car/:id',(req, res) => {
	let car = null
	let carIndex = 0
	let id = req.params.id
	for (var i = cars.length - 1; i >= 0; i--) {
		if(cars[i].id == id)
		{
			car = cars[i]
			carIndex = i;
		}
	}
	if(car == null)
		{
			res.status(404)
		}
	else
		{
			if(req.body.make){
				cars[carIndex].make = req.body.make
			}
			if(req.body.model){
				cars[carIndex].model = req.body.model
			}
			if(req.body.year){
				cars[carIndex].year = req.body.year
			}
			res.status(200)
		}
	res.send(cars[carIndex])


})

//Delete
app.delete('/car/:id',(req, res) => {
	let car = null
	let id = req.params.id

	res.status(404)
	for (var i = cars.length - 1; i >= 0; i--) {
		if(cars[i].id == id)
		{
			cars[i] = {"id": id}
			res.status(200)
		}
	}
	res.end()
})

app.listen(PORT, () => { console.log(" // super // car API started")})